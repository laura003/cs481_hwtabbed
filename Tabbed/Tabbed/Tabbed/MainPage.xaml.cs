﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tabbed
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : TabbedPage
    {
        public ObservableCollection<Contacts> Contacts = new ObservableCollection<Contacts>()
        {
            new Contacts()                               //
            {                                            //
                Name = "Jean",                           //
                Number = "+1 (702) 861-6102",            // Add new class contacts in Contacts ObservableCollection
                Favorite = true,                         //
                FavIcon = FavIcon(true),                 //
                Recent = false,                          //
            },
        new Contacts()
            {
                Name = "Steve Jobs",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = true
            },
            new Contacts()
            {
                Name = "Alan Turing",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = false
            },
            new Contacts()
            {
                Name = "Steve Wozniak",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = true
            },
            new Contacts()
            {
                Name = "Albert Einstein",
                Number = "+1 (702) ***-****",
                Favorite = true,
                FavIcon = FavIcon(true),
                Recent = false
            },
            new Contacts()
            {
                Name = "Marie Curie",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = false
            },
            new Contacts()
            {
                Name = "Isaac Newton",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = true
            },
            new Contacts()
            {
                Name = "Louis Pasteur",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = false
            },
            new Contacts()
            {
                Name = "Stephen Hawking",
                Number = "+1 (702) ***-****",
                Favorite = true,
                FavIcon = FavIcon(true),
                Recent = true
            },
            new Contacts()
            {
                Name = "Blaise Pascal",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = false
            },
            new Contacts()
            {
                Name = "Thomas Edison",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = false
            },
            new Contacts()
            {
                Name = "Michel Faraday",
                Number = "+1 (702) ***-****",
                Favorite = true,
                FavIcon = FavIcon(true),
                Recent = false
            },
            new Contacts()
            {
                Name = "Nikola Tesla",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = false
            },
            new Contacts()
            {
                Name = "Charles Darwin",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = true
            },
            new Contacts()
            {
                Name = "Max Planck",
                Number = "+1 (702) ***-****",
                Favorite = true,
                FavIcon = FavIcon(true),
                Recent = false
            },
            new Contacts()
            {
                Name = "Leonard de Vinci",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = true
            },
            new Contacts()
            {
                Name = "Heinrich Hertz",
                Number = "+1 (702) ***-****",
                Favorite = false,
                FavIcon = FavIcon(false),
                Recent = true
            },
            new Contacts()
            {
                Name = "Georg Ohm",
                Number = "+1 (702) ***-****",
                Favorite = true,
                FavIcon = FavIcon(true),
                Recent = false
            },
            new Contacts()
            {
                Name = "James Watt",
                Number = "+1 (702) ***-****",
                Favorite = true,
                FavIcon = FavIcon(true),
                Recent = false
            }
        };
        public MainPage()
        {
            InitializeComponent();

            Children.Add(new Favorites(Contacts)); //Add Favorites Tabbed
            Children.Add(new ListContact(Contacts)); //Add ListContact Tabbed
            Children.Add(new Recents(Contacts)); //Add Recents Tabbed
        }
        static string FavIcon(bool favorite)
        {
            return favorite == true ? "round_favorite_black_24dp.png" : "baseline_favorite_border_black_24dp.png"; //Choose the correct pnj
        }

        private void TabbedPage_Appearing(object sender, EventArgs e)
        {
            DisplayAlert("Hello", "Welcome to my Phonebook", "Ok"); //Send a message  when this page appear
        }
    }
}
