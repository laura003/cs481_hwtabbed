﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tabbed
{
    public class Contacts
    {
        public string Name { get; set; }            //
        public string Number { get; set; }          //
        public string FavIcon { get; set; }         // Element for my Contacts class
        public bool Favorite { get; set; }          //
        public bool Recent { get; set; }            //

        public override string ToString()
        {
            return Name;
        }
    }
}
