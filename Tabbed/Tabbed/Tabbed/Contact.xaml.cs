﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Contact : ContentPage
    {
        public Contacts contacts { get; private set; }
        public Contact(Contacts contact)
        {
            InitializeComponent();
            contacts = new Contacts(); //
            contacts = contact;        // Create context for binding
            BindingContext = this;     //
        }
    }
}