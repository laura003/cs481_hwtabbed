﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Keypad : ContentPage
    {
        public Keypad()
        {
            InitializeComponent();
        }
        void OnClickBtn(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            this.number.Text += button.Text; // Add button text to my number label
        }

        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            this.number.Text = ""; //Set Number label to "" when this page disappear
        }
    }
}