﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Recents : ContentPage
    {
        public ObservableCollection<Contacts> contacts { get; private set; }
        public Recents(ObservableCollection<Contacts> Contacts)
        {
            InitializeComponent();
            contacts = new ObservableCollection<Contacts>();
            var tmp = Contacts.Where(fav => fav.Recent == true).ToList(); //Only select my recent call
            foreach (var fav in tmp)
            {
                contacts.Add(fav); //Add for display
            }
            BindingContext = this;
        }
        void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        { 
            Contacts selectedItem = e.SelectedItem as Contacts; //Select item click
        }

        void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            Contacts tappedItem = e.Item as Contacts; //Select item tapped
            Navigation.PushAsync(new Contact(tappedItem)); // Lunch the tappedItem Page
        } 
    }
}