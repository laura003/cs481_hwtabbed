﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Favorites : ContentPage
    {
        public ObservableCollection<Contacts> contacts { get; private set; }
        public Favorites(ObservableCollection<Contacts> Contacts)
        {
            InitializeComponent();
            contacts = new ObservableCollection<Contacts>();
            var tmp = Contacts.Where(fav => fav.Favorite == true).ToList(); // Only select my favorite contact
            foreach(var fav in tmp)
            {
                contacts.Add(fav); //Add to contact lObservableCollection for display
            }
            BindingContext = this;
        }
        void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Contacts selectedItem = e.SelectedItem as Contacts; //Select item click
        }

        void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            Contacts tappedItem = e.Item as Contacts; //Select item tapped
            Navigation.PushAsync(new Contact(tappedItem)); // Lunch the tappedItem Page
        }
    }
}